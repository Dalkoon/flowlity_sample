# One Page Service with Dropdown Dashboard

![Main overview](demo/1.gif)

## Demo
You will find a running demo http://flowlity.ngrok.io

## Context
A problem suggested by  FLowlity in order to apply a Senior Developper position, : https://www.notion.so/Software-engineer-exercise-ce350cc3bd814601bbfcb40841afc1cb
The main technical parts that are showcased: 1) Dataengineering :building an app 2) Backend Rest Api communication 3) Get and Post usage

## Main objectif
The User want to know inventory level evolution for a  specific product, the main oriented is to build up micro service for inventory purposes :
 * Product check up : Ability to check product status
 * Update product informations

## Features:
![Dropdown](demo/2.gif)
#####  Dropdown option to choose the product ID or product name to visualize
![Graphs](demo/3.gif)
#####  A graph of inventory level(y-axis) vs. date(x-axis) of the selected product
![Tables](demo/4.gif)
#####  Table of data filtered on the selected product (product_id; product_name; date; inventory_level)
![edit Table](demo/5.gif)
##### Edit from table and update database

## Prerequisites:
* Linux or Debian
* Dockers https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04
* Ngrok authtoken and Tunnel name https://dashboard.ngrok.com/get-startedauthifciation token and tunnel name


## Technological choice
##### Back end
* Docker http://containertutorials.com/docker-compose/flask-simple-app.html
* Production proof library choices https://stackify.com/docker-performance-improvement-tips-and-tricks/
* Mongo https://github.com/dockerfile/mongodb
* Flask using copy pastes from Main build is inspired https://cookiecutter.readthedocs.io/en/latest/

##### Front end
* Jquery and ajax https://api.jquery.com/jquery.ajax/
* Plotly https://plot.ly/javascript/
* Handsontable https://handsontable.com/examples?manual-resize&manual-move&conditional-formatting&context-menu&filters&dropdown-menu&headers
* Dropdown https://stackoverflow.com/questions/50200327/only-plot-new-dropdown-selection-using-jquery-and-plotly-js

## Installation

##### Modify authtoken and tunnel name for ngrok

https://dashboard.ngrok.com/get-startedauthifciation get token and tunnel name
Modifiy flowsample_master/scripts_testing/ngrok_start.sh with authotken and tunnel name created from ngrok

##### Get repository and launch docker image
```console
wget https://gitlab.com/Dalkoon/flowlity_sample/-/archive/master/flowlity_sample-master.zip

unzip flowlity_sample-master

cd flowlity_sample-master

docker build -t serverimage:latest .

docker run -d -p - name=flowlity 5010:5010 serverimage
```
##### Debugging purpose
stop
```console
sudo docker stop flowlity
```
remove
```console
sudo docker rm flowlity
```
Checking logs
```console
docker logs flowlity
```

## Main scripts
Dockerfile: All requirements for webserver launch, mongodb etc..

Webapp.py: To build the server and add extensions

Views.py: To build the client side views

One_page.html: Html and ajax

##  Next steps

* Unit tests
* Add constraints  to editing
* Use Sockets to broadcast changes


Homemade by Tarik El Kalai

Problem suggested by <img src="https://static.wixstatic.com/media/11bd98_e71d20318f0649df9a01536a18aab8c9~mv2_d_5000_5000_s_4_2.png/v1/crop/x_0,y_1685,w_5000,h_1630/fill/w_216,h_73,al_c,q_80,usm_0.66_1.00_0.01/high-res.webp" align="top"
     title="" width="120" >


