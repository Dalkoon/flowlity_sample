# Envirnomment memoire
import sys
sys.path.append("/home/tarik/kchatmedia/Kchat/kchat/utils")
sys.path.append("/home/tarik/Kchatmedia/Kchat/Utils")
# Imports
from koachwebapp.app import create_app
from koachwebapp.settings import DevConfig, ProdConfig
import os
import re
import numpy
from koachwebapp.database import db
import datetime,random,time
from subprocess import call
from flask_socketio import SocketIO, send, emit
import json
from threading import Thread,Event
import eventlet
eventlet.monkey_patch()
app = create_app(DevConfig)
socket_io = SocketIO(app)
# Import librairies
import os,sys,socket
import numpy as np
import time
from random import randint
import pandas as pd



## Preparation des indicateurs
import math
def weighted_avg_and_std(listdata,key):
    """
    Return the weighted average and standard deviation.

    values, weights -- Numpy ndarrays with the same shape.
    """
    values=[]
    for j in listdata:
        values.append(j[key])
    alpha=2.5
    beta=2.5
    weights=[]
    for i in values:
        weights.append(beta)
        beta=beta*alpha
    tot=numpy.sum(weights)
    weights_new=[]
    for i in weights:
        weights_new.append(i/tot)
    average = numpy.average(values, weights=weights)
    # Fast and numerically precise:
    variance = numpy.average((values-average)**2, weights=weights)
    return (average, math.sqrt(variance))
# Creation du thread de reception
thread = Thread()
thread_stop_event = Event()
class Sensor(Thread):
    def __init__(self):
        self.delay = 0.10
        super(Sensor, self).__init__()
    def udp_sensor(self):
        while True:
                    # Preparation dictionnaire et envoi
                dict_data = {"client":names.get_full_name(),"id": np.random.randint(150)}

                socket_io.emit('zedata', dict_data)
                time.sleep(3)
    def run(self):
        self.udp_sensor()


def launch_thread():
    # need visibility of the global thread object
    global thread
    print('Client connected')
    #Start the random number generator thread only if the thread has not been started before.
    if not thread.isAlive():
        print("Starting Thread")
        #thread = QR()
        #thread.start()
        thread = Sensor()
        thread.start()



if __name__ == '__main__':
    #launch_thread()
    port = int(os.environ.get('PORT', 5010))
    socket_io.run(app, port=port)




# Dash est problématique lorsqu'il doit communiquer avec le reste de falsk, j'ai pris un exempe avec les sockets pour mettre le slimitations de ce type de libraiie idéal pour du prootypage  mais très risqué ne production, uil ya des moyens de le fire passer ne production si vraiment c'est incoutournable mais cen'ets pas trivial  https://medium.com/@olegkomarov_77860/how-to-embed-a-dash-app-into-an-existing-flask-app-ea05d7a2210b