######## Docker image import
# Ubuntu in linux we trust
FROM ubuntu:latest
# Anaconda installing
FROM continuumio/miniconda3
######## Ubuntu
RUN apt-get update &&  yes|apt-get upgrade
# Updating Ubuntu packages
RUN apt-get install -y emacs
RUN apt-get install -y gnupg
RUN apt-get install -y gnupg2
RUN apt-get install -y wget
RUN apt-get install -y rng-tools
# Adding wget and bzip2
RUN apt-get install -y wget bzip2
RUN apt-get install -y unzip
RUN apt-get install -y supervisor
RUN apt-get install -y php-xmlrpc

######## Database Mongodb
#https://medium.com/@pablo_ezequiel/creating-a-docker-image-with-mongodb-4c8aa3f828f2
# Add the package verification key
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
# Add MongoDB to the repository sources list
RUN echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | tee /etc/apt/sources.list.d/mongodb.list
# Install MongoDB package (.deb)
RUN apt-get install -y mongodb
# Create the default data directory
RUN mkdir -p /data/db
# Expose the default port
EXPOSE 5010 27017

######## Python packages
RUN conda create -n env python=3.6
ENV PATH /opt/conda/envs/env/bin:$PATH
# Updating Anaconda packages
RUN conda update conda
RUN conda update --all
RUN conda install -c anaconda pip
RUN conda install -c anaconda gevent-websocket
RUN conda install -c anaconda ujson
RUN conda install -c anaconda flask
RUN conda install -c nsidc flask-sockets
RUN conda install -c conda-forge wsaccel
RUN conda install -c conda-forge astor


######## App requirements
RUN pip install pip==9.0.1
COPY . /Flowlity_sample
WORKDIR /Flowlity_sample
RUN pip install -r requirements/requirements.txt
RUN pip install Flask-Analytics
RUN pip install flask-bootstrap
RUN pip install flask_datepicker
RUN pip install Flask-Compress
RUN pip install Flask-MongoAlchemy
RUN pip install numpy
RUN pip install Flask-Minify
RUN pip install flask-ngrok
RUN pip install Flask-SocketIO
RUN pip install eventlet
RUN pip install pandas
RUN pip install names
RUN pip install pypi-xmlrpc
RUN pip install Random-Word

######## Service management : Supervisor
# Install and setup supervisor
RUN pip install supervisor
RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
######## Unit Tests,
#  Test before you finish building, we will not get in docker container management
#RUN mongod
#RUN sh scripts_testing/ngrok_start.sh
#RUN sh scripts_testing/bot_start.sh
#RUN supervisord

######## Command me bro
# Launching server, the tunnel and the app
# Launch supervisor
CMD ["supervisord"]