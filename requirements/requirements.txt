# Everything needed in production

# Flask
Flask==0.12.2
MarkupSafe==1.0
Werkzeug==0.12.2
Jinja2==2.9.6
itsdangerous==0.24

# Database
Flask-SQLAlchemy==2.2
psycopg2==2.7.3
SQLAlchemy==1.1.11

# Migrations
Flask-Migrate==2.0.4

# Forms
Flask-WTF==0.14.2
WTForms==2.1

# Deployment
gunicorn>=19.1.1

# Webpack
flask-webpack==0.1.0

# Auth
Flask-Login==0.4.0
Flask-Bcrypt==0.7.1

# Caching
Flask-Caching>=1.0.0

# Debug toolbar
Flask-DebugToolbar==0.10.1
asn1crypto==0.22.0
certifi==2017.7.27.1
cffi==1.10.0
chardet==3.0.4
click==6.7
cryptography==2.0.3
enum34==1.1.6
Flask==0.12.2
idna==2.6
ipaddress==1.0.18
itsdangerous==0.24
Jinja2==2.9.6
MarkupSafe==1.0
ndg-httpsclient==0.4.3
pusher==1.7.2
pyasn1==0.3.4
pycparser==2.18
pyOpenSSL==17.2.0
requests==2.18.4
six==1.10.0
urllib3==1.22
Werkzeug==0.12.2