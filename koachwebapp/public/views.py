# -*- coding: utf-8 -*-
############ libraries import
import sys
from flask import Blueprint, flash, redirect, render_template, request, url_for,jsonify
from flask_login import login_required, login_user, logout_user
from pusher import Pusher
from koachwebapp.extensions import login_manager,db,bcrypt
import random
import json
import time
import numpy as np
#  Reception et envoi en socket des données du capteur
## Preparation de la base de donnees et des tables d'enregistrement
from pymongo import MongoClient
client = MongoClient() # Server mongodb
db = client["Inventory"]
table = db["Client1"]

############  needed functions
#https://pypi.org/project/Random-Word/
from random_word import RandomWords
randomwordengine = RandomWords()
# random time generation
# documentation here https://python-decompiler.com/article/2009-02/generate-a-random-date-between-two-other-dates
db = client["Inventory"]
table = db["Client1"]
def strTimeProp(start, format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """
    stime = time.mktime(time.strptime(start, format))
    ptime = stime + prop * (500000)
    return time.strftime(format, time.localtime(ptime))
def randomDate(start):
    prop=random.random()
    return strTimeProp(start, '%Y-%m-%d %I:%M', prop)
# Each product have a data log with inventory level and date
def add_random_date_and_inventory(Product_inventory,start):
    Inventory = {}
    Inventory["date"]=randomDate(start)
    Inventory["inventory_level"]=np.random.randint(100000)
    Product_inventory["Inventory"].append(Inventory)
    return Product_inventory,Inventory["date"]
# Generate a set of product
def generate_data_id_and_name():
    Product_inventory={}
    Product_inventory["id"]=np.random.randint(9999999999999999) # normalement il faudrait un generateur de clé unique mais mathematiquement celui-ci ets déjà très robuste
    Product_inventory["name"]=randomwordengine.get_random_word()
    Product_inventory["Inventory"]=[]
    return Product_inventory
# Generate the global table with dates
def generate_table_products_inventory(numberofproducts=40, numberofdates=100):
    Warehouse_inventory=[]
    for k in range(numberofproducts):
        Product_inventory=generate_data_id_and_name()
        start = "2008-1-1 1:30"
        for i in range(numberofdates):
            Product_inventory,start=add_random_date_and_inventory(Product_inventory,start)

        Warehouse_inventory.append(Product_inventory)
    return Warehouse_inventory
# Import data if doesnt exist generate it
def import_or_create():
    tableimport = []
    for k in table.find():
        tableimport.append(k)
    if tableimport==[]:
        tableimport=generate_table_products_inventory()
        for k in tableimport:
            table.save(k)
    for k in tableimport:
            k.pop('_id', None)
    return tableimport
def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ))

############  Blueprint management for views.py in folder public
blueprint = Blueprint('public', __name__, static_folder='../static')

############  view function
@blueprint.route('/', methods=['GET', 'POST'])
def landing():
    tableimport = import_or_create()
    form = dict(request.form)
    form['data']=tableimport
    form['listproducts'] = []
    for product in tableimport:
        form['listproducts'].append(product["name"])
    form['data']=jsonify(form['data'])
    form['listproducts']=jsonify(form['listproducts'])
    #data= import_donne
    # Handle logging in
    if request.method == 'POST':
        print(form.validate_on_submit())
        if form.validate_on_submit():
            #data= cahnger la donnee
            return render_template('public/One_page_tool.html', form=form)
        else:
            flash_errors(form)
    else:
        return render_template('public/One_page_tool.html',form=form)

############ Réception des requetes Get de l'ajax
@blueprint.route('/data/', methods= ['GET' , 'POST'])
def data(table=table):
    tableimport = import_or_create()
    if request.method == 'POST':
        data=request.get_json()
        print(data["change"])
        for k in table.find({"name":data["product"]}):
            if data["change"][0][1]==0:
                k["Inventory"][data["change"][0][0]]['date']=data["change"][0][3]
            if data["change"][0][1]==1:
                k["Inventory"][data["change"][0][0]]['inventory_level']=data["change"][0][3]
            table.save(k)
    form = dict(request.form)
    form['data'] = tableimport
    form['listproducts'] = []
    for product in tableimport:
        form['listproducts'].append(product["name"])
    form['data'] = jsonify(form['data'])
    all_time_series={}
    for product in tableimport:
        time_series_dates=[]
        time_series_inventory_level=[]
        for element in product["Inventory"]:
            time_series_dates.append(element["date"])
            time_series_inventory_level.append(element["inventory_level"])
        all_time_series[product["name"]]=(time_series_dates,time_series_inventory_level)
    data_inventory={}
    for product in tableimport:
        table=[]
        for element in product["Inventory"]:
            table.append([str(element["date"]),str(element['inventory_level'])])
        data_inventory[product["name"]]=table
    return jsonify(listofproducts=form['listproducts'],all_time_series=all_time_series,data_inventory=data_inventory)