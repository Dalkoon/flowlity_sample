var timer = new Timer();
timer.start({countdown: true, startValues: {seconds: parseInt(secondsvariable) }});
$('#countdownExample .values').html(timer.getTimeValues().toString());
timer.addEventListener('secondsUpdated', function (e) {
    $('#countdownExample .values').html(timer.getTimeValues().toString());
});
timer.addEventListener('targetAchieved', function (e) {
    $('#countdownExample .values').html('Fini !');
});