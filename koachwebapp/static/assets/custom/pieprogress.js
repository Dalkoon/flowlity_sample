jQuery(function ($) {
        $('.pie_progress').asPieProgress({
            namespace: 'pie_progress'
        });
        // Example with grater loading time - loads longer
        $('.pie_progress--slow').asPieProgress({
            namespace: 'pie_progress',
            goal: 1000,
            min: 0,
            max: 1000,
            speed: 200,
            easing: 'linear'
        });
        // Example with grater loading time - loads longer
        $('.pie_progress--countdown').asPieProgress({
            namespace: 'pie_progress',
            easing: 'linear',
            first: 120,
            max: 120,
            barcolor: '#000000',
            goal: 0,
            speed: 1200, // 120 s * 1000 ms per s / 100
            numberCallback: function (n) {
                var minutes = Math.floor(this.now / 60);
                var seconds = this.now % 60;
                if (seconds < 10) {
                    seconds = '0' + seconds;
                }
                return minutes + ': ' + seconds;
            }
        });

         function used_modules() {
        $.ajax({
            type: "GET",
            url: "/users/admin_json_global",
            success: function (data) {
                var users = $.parseJSON(data)
                var nbr_users = parseFloat(users['nbr_users'])
                var bla_bla = Math.round(parseFloat(users['used_modules_dict']['bla_bla']))
                var exercise = Math.round(parseFloat(users['used_modules_dict']['Exercices']))
                var pas_compris = Math.round(parseFloat(users['used_modules_dict']['Pas compris']))
                var diagnostic = Math.round(parseFloat(users['used_modules_dict']['Diagnostic']))
                $('#pie_progressp').asPieProgress('go', nbr_users);
            }
        }).then(function () {           // on completion, restart
            setTimeout(used_modules, 10000);  // function refers to itself
        });
    };
used_modules();

        $('#button_start').on('click', function () {
            $('.pie_progress').asPieProgress('start');
        });
        $('#button_finish').on('click', function () {
            $('.pie_progress').asPieProgress('finish');
        });
        $('#button_go').on('click', function () {
            $('.pie_progress').asPieProgress('go', 50);
        });
        $('#button_go_percentage').on('click', function () {
            $('.pie_progress').asPieProgress('go', '50%');
        });
        $('#button_stop').on('click', function () {
            $('.pie_progress').asPieProgress('stop');
        });
        $('#button_reset').on('click', function () {
            $('.pie_progress').asPieProgress('reset');
        });
    });