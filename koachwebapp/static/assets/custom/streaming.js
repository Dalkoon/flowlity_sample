(function (document, window, $) {
    'use strict';

    var Site = window.Site;

    $(document).ready(function ($) {
        Site.run();
    });
//lancer l'appel ajax de récupération de données
    $(document).ready(function ($) {
        var chart = c3.generate({

            bindto: '#stab_ante_rt',
            data: {
                columns: [
                    ['data', 0]
                ],

                type: 'gauge',
                color: {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold: {
                        values: [30, 100, 200, 300]
                    }
                },
                size: {
                    height: 180
                }
            }});

        var chart1 = c3.generate({

            bindto: '#stab_taille_rt',
            data: {
                columns: [
                    ['data', 0]
                ],
                type: 'gauge',
                color: {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold: {
                        values: [30, 100, 200, 300]
                    }
                },
                size: {
                    height: 180
                }
            }});

        var chart2 = c3.generate({

            bindto: '#stab_lateral_rt',
            data: {
                columns: [
                    ['data', 0]
                ],
                type: 'gauge',
                color: {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold: {
                        values: [30, 100, 200, 300]
                    }
                },
                size: {
                    height: 180
                }
            }});

        var chart3 = c3.generate({

            bindto: '#stab_ante_moy',
            data: {
                columns: [
                    ['data', 0]
                ],
                type: 'gauge',
                color: {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold: {
                        values: [30, 100, 200, 300]
                    }
                },
                size: {
                    height: 180
                }
            }});

        var chart4 = c3.generate({

            bindto: '#stab_taille_moy',
            data: {
                columns: [
                    ['data', 0]
                ],
                type: 'gauge',
                color: {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold: {
                        values: [30, 100, 200, 300]
                    }
                },
                size: {
                    height: 180
                }
            }});

        var chart5 = c3.generate({

            bindto: '#stab_lateral_moy',
            data: {
                columns: [
                    ['data', 0]
                ],
                type: 'gauge',
                color: {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold: {
                        values: [30, 100, 200, 300]
                    }
                },
                size: {
                    height: 180
                }
            }});



        setInterval(function () {
            $.ajax({
                type: "GET",
                url: "/users/start",
                cache: false,
                async: false,
                success: function (data) {
                    var sensor_d = $.parseJSON(data);

                    var stab_ante_rt = parseInt(sensor_d['stab_ante_rt']);
                    var stab_taille_rt = parseInt(sensor_d['stab_taille_rt']);
                    var stab_lateral_rt = parseInt(sensor_d['stab_lateral_rt']);
                    var stab_ante_moy = parseInt(sensor_d['stab_ante_moy']);
                    var stab_taille_moy = parseInt(sensor_d['stab_taille_moy']);
                    var stab_lateral_moy = parseInt(sensor_d['stab_lateral_moy']);

                    chart.load({
                        columns: [['data', stab_ante_rt]]
                    });
                    chart1.load({
                        columns: [['data', stab_taille_rt]]
                    });
                    chart2.load({
                        columns: [['data', stab_lateral_rt]]
                    });
                    chart3.load({
                        columns: [['data', stab_ante_moy]]
                    });
                    chart4.load({
                        columns: [['data', stab_taille_moy]]
                    });
                    chart5.load({
                        columns: [['data', stab_lateral_moy]]
                    });


                }
            });
        }, 500);
    });
})(document, window, jQuery);
function get_sensor_data() {
    $.ajax({
            type: "GET",
            url: "/users/start",
            success: function (data) {
                var sensor_d = $.parseJSON(data);

                var stab_ante_rt = parseFloat(sensor_d['stab_ante_rt']);
                var stab_taille_rt = parseFloat(sensor_d['stab_taille_rt']);
                var stab_lateral_rt = parseFloat(sensor_d['stab_lateral_rt']);
                var stab_ante_moy = parseFloat(sensor_d['stab_ante_moy']);
                var stab_taille_moy = parseFloat(sensor_d['stab_taille_moy']);
                var stab_lateral_moy = parseFloat(sensor_d['stab_lateral_moy']);
                console.log(stab_ante_rt);
                var dynamicGauge1 = $("#stab_ante_rt").data('gauge');
                var dynamicGauge2 = $("#stab_taille_rt").data('gauge');
                var dynamicGauge3 = $("#stab_lateral_rt").data('gauge');
                var dynamicGauge4 = $("#stab_ante_moy").data('gauge');
                var dynamicGauge5 = $("#stab_taille_moy").data('gauge');
                var dynamicGauge6 = $("#stab_lateral_moy").data('gauge');

                var options = {
                    strokeColor: Config.colors("primary", 500)
                };


                dynamicGauge1.setOptions(options);
                dynamicGauge1.set(stab_ante_rt);
                dynamicGauge2.setOptions(options);
                dynamicGauge2.set(stab_taille_rt)
                dynamicGauge3.setOptions(options);
                dynamicGauge3.set(stab_lateral_rt);
                dynamicGauge4.setOptions(options);
                dynamicGauge4.set(stab_ante_moy);
                dynamicGauge5.setOptions(options);
                dynamicGauge5.set(stab_taille_moy);
                dynamicGauge6.setOptions(options);
                dynamicGauge6.set(stab_lateral_moy);

            }
        }
    ).then(function () {           // on completion, restart
        setTimeout(get_sensor_data, 10);  // function refers to itself
    });
}


//au clic sur start, lancer le timer

//au clic sur start, lancer l'appel ajax pour récupérer les données RT et Moye du capteur et afficher gauges

// au clic sur record, lancer le timer, ajax get data, ajax, record data
//au clic sur stop, arrêter le timer

//au clic sur stop, arrêter l'appel ajax, récupérer les données de synthèse et afficher le chart synthèse

//au clic sur next, view url prochain exercice

//au clic sur finish, arrêter le timer

//au clic sur finish,
// controllers
jQuery(function ($) {
    $('.progress').asProgress({
        'namespace': 'progress',
        'bootstrap': false,
        'min': 0,
        'max': 100,
        'goal': 100,
        'speed': 600
    });
    $('#button_start').on('click', function () {
        $('.progress').asProgress('start');

    });
    $('#button_finish').on('click', function () {
        $('.progress').asProgress('finish');
    });

    $('#button_stop').on('click', function () {
        $('.progress').asProgress('stop');
    });
    $('#button_reset').on('click', function () {
        $('.progress').asProgress('reset');
    });
});