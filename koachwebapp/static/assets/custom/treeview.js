function display_main_tasks(){
    $.ajax({
        type: "GET",
        url: "/users/get_main_tasks",
        success: function (data) {

            var main_tasks = $.parseJSON(data);
            var html ="";
            for (i=0;i<main_tasks.length; i++){
                html+="<li class='list-group-item' data-plugin='tasklist'>"+main_tasks[i]+"</li>";


            }
            $('#maintasks').html(html);
        }


    });
}

function get_tree_data() {
    $.ajax({
        type: "GET",
        url: "/users/tree_data",
        success: function (data) {
            var tree = $.parseJSON(data);

            $('#tree').treeview({data: tree});
        }
    });
}
get_tree_data();
display_main_tasks();

$(function () {
    $('#tree').contextMenu({
        selector: 'li',
        callback: function (key, options) {
            //var m = "clicked: " + key + " on " + $(this).text();
            //window.console && console.log(m) || alert(m);
            if (key == "add") {
                $('#addStageFrom').modal();
                return true;
            }
            if (key == "edit") {
                $('#editStageFrom').modal();
                console.log("edit");
                return true;
            }
            if (key == "delete") {
                $('#deleteStageFrom').modal();
                return true;
            }
        },
        items: {
            "add": {name: "Ajouter une tâche"},
            "edit": {name: "Modifier une tâche"},
            "delete": {name: "Supprimer une tâche"}
        }
    });
});

function add_task() {
    var task_name = document.getElementById("task_name").value;
    var tag_task_tobeadded = $('#tree').treeview('getSelected')[0]['tags'];
    $.ajax({
        type: "GET",
        url: "/users/add_task",
        data: {"task_name": task_name,"tag":tag_task_tobeadded} ,
        success: function (data) {

            var tree = $.parseJSON(data);
            $('#tree').treeview({data: tree});
        }


    });
}

function modify_task() {
    var task_tobemodified = $('#tree').treeview('getSelected')[0]['text'];
    var tag_task_tobemodified = $('#tree').treeview('getSelected')[0]['tags'];
    var task_name = document.getElementById("modified_task_name").value;
    console.log($('#tree').treeview('getSelected')[0] );

    $.ajax({
        type: "GET",
        url: "/users/modify_task",
        data: {"edited_task": task_tobemodified, "tag": tag_task_tobemodified, "new_task": task_name},
        success: function (data) {

            var tree = $.parseJSON(data);
            $('#tree').treeview({data: tree});

        }


    });
}

function delete_task() {
    var task_tobedeleted = $('#tree').treeview('getSelected')[0]['text'];
    var tag_task_tobedeleted = $('#tree').treeview('getSelected')[0]['tags'];
    console.log(tag_task_tobedeleted);
    $.ajax({
        type: "GET",
        url: "/users/delete_task",
        data: {"deleted_task": task_tobedeleted, "tag": tag_task_tobedeleted},
        success: function (data) {

            var tree = $.parseJSON(data);
            $('#tree').treeview({data: tree});
        }


    });
}

$("#addtask").click(function () {
        add_task();
    }
);

$("#modifytask").click(function () {
        modify_task();
    }
);

$("#deletetask").click(function () {
        delete_task();
    }
);

$('#tree').on('nodeSelected', function (event, data) {
    $("#task_tobemodified").ready(function () {
        var task_tobe_edited = $('#tree').treeview('getSelected')[0]['text'];
        $("#task_tobemodified").append(document.createTextNode(task_tobe_edited));
    });
});


$('#tree').on('nodeSelected', function (event, data) {
    console.log("houpi");
    $("#task_tobedeleted").ready(function () {
        var task_tobedeleted = $('#tree').treeview('getSelected')[0]['text'];
        $("#task_tobedeleted").append(document.createTextNode(task_tobedeleted));
    });
});


