

function post_financial_data() {
    var financement = document.getElementById("financement").value;
    console.log(financement);
    var parts = document.getElementById("parts").value;
    console.log(parts);
    var rd_costs = document.getElementById("rd_costs").value;
    console.log(rd_costs);
    var marketing_costs = document.getElementById("marketing_costs").value;
    console.log(marketing_costs);
    var acquisition_costs = document.getElementById("acquisition_costs").value;
    console.log(acquisition_costs);
    var average_basket = document.getElementById("average_basket").value;
    console.log(average_basket);
    var production_costs = document.getElementById("production_costs").value;
    console.log(production_costs);
    console.log("ok then");
    $.ajax({
        type: "GET",
        url: "/users/financial_data",
        data: {
            "financement": parseFloat(financement),
            "parts": parseFloat(parts),
            "rd_costs":parseFloat(rd_costs),
            "marketing_costs": parseFloat(marketing_costs),
            "acquisition_costs":parseFloat(acquisition_costs),
            "average_basket":parseFloat(average_basket),
            'production_costs': parseFloat(production_costs)
        },
        success: function (data) {
            var pnls = $.parseJSON(data);
            var pnl = pnls["pnl"]


            var values = [parseFloat(pnl[0]), parseFloat(pnl[1]), parseFloat(pnl[2]), parseFloat(pnl[3]), parseFloat(pnl[4])];
            var info = {
                'choices': ['2017', '2018', '2019', '2020', '2021'],
                'values': values
            };

            console.log(info);
            var data = [
  {
    x: [info['choices'][0],info['choices'][1], info['choices'][2], info['choices'][3], info['choices'][4]],
    y: values,
    type: 'bar'
  }
];

Plotly.newPlot('myChart2', data);



        }
    });
};
post_financial_data();
$("#fbutton2").click(function () {
    post_financial_data();
});
$("#financement").change(function () {
    post_financial_data();
});

$("#marketing_costs").knob({

        'change' : function (v) { console.log("change");

    document.getElementById("rd_costs").value = 100 - parseInt(v) ;
        post_financial_data();
        }
    });
$("#rd_costs").knob({
        'change' : function (v) { console.log("change");

    document.getElementById("marketing_costs").value = 100 - parseInt(v) ;
        post_financial_data();
        }
    });
$("#parts").knob({
        'change' : function (v) { console.log("change");
        post_financial_data();
        }
    });

$("#acquisition_costs").knob({
        'change' : function (v) { console.log("change");
        post_financial_data();
        }
    });

$("#production_costs").knob({
        'change' : function (v) { console.log("change");
        post_financial_data();
        }
    });
$("#average_basket").knob({
        'change' : function (v) { console.log("change");
        post_financial_data();
        }
    });