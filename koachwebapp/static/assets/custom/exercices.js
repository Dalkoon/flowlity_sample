function lancer_test() {

    $.ajax({
        type: "GET",
        url: "/users/start",
        data: {},
        success: function (data) {
            console.log("success");

        }
    });
};

function stop_test() {

    $.ajax({
        type: "GET",
        url: "/users/stop",
        data: {},
        success: function (data) {
            console.log("success");
            var json = $.parseJSON(data);
            for (i = 0; i < json_list.length; i++) {
                json = json_list[i];
                x_timestamp.push(json['timestamp']);
                MoyenneGx.push(json['MoyenneGx']);
                MoyenneGy.push(json['MoyenneGy']);
                MoyenneGz.push(json['MoyenneGz']);
                stdGx.push(json['stdGx']);
                stdGy.push(json['stdGy']);
                stdGz.push(json['stdGz']);
            }
            var data1 = [
                {
                    x: x_timestamp,
                    y: MoyenneGx,
                    type: 'scatter'
                }
            ];

            Plotly.newPlot('myChart1', data1);

            var data2 = [
                {
                    x: x_timestamp,
                    y: MoyenneGy,
                    type: 'scatter'
                }
            ];

            Plotly.newPlot('myChart2', data2);

            var data3 = [
                {
                    x: x_timestamp,
                    y: MoyenneGz,
                    type: 'scatter'
                }
            ];

            Plotly.newPlot('myChart3', data3);

            var data4 = [
                {
                    x: x_timestamp,
                    y: stdGx,
                    type: 'scatter'
                }
            ];

            Plotly.newPlot('myChart4', data4);

            var data5 = [
                {
                    x: x_timestamp,
                    y: stdGy,
                    type: 'scatter'
                }
            ];

            Plotly.newPlot('myChart5', data5);

            var data6 = [
                {
                    x: x_timestamp,
                    y: stdGz,
                    type: 'scatter'
                }
            ];

            Plotly.newPlot('myChart6', data6);

        }
    });
};

$("#lancer_test").click(function () {
    lancer_test();
});

$("#stop_test").click(function () {
    stop_test();
});