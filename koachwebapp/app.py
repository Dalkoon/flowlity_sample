# -*- coding: utf-8 -*-
"""The app module, containing the app factory function."""
from flask import Flask, render_template
from flask_analytics import Analytics
from flask_bootstrap import Bootstrap
from flask_datepicker import datepicker
from flask.ext.compress import Compress
from flask.ext.mongoalchemy import MongoAlchemy
from koachwebapp import commands, public, user
from koachwebapp.extensions import bcrypt, cache, csrf_protect, db, debug_toolbar, login_manager, migrate, webpack
from koachwebapp.settings import ProdConfig
from flask_minify import minify
from flask_ngrok import run_with_ngrok
compress = Compress()




def create_app(config_object=ProdConfig):
    """An application factory, as explained here: http://flask.pocoo.org/docs/patterns/appfactories/.

    :param config_object: The configuration object to use.
    """
    app = Flask(__name__.split('.')[0])
    app.config.from_object(config_object)
    app.config['MONGOALCHEMY_DATABASE'] = 'Webapp'
    db = MongoAlchemy(app)
    csrf = csrf_protect
    csrf.init_app(app)
    register_extensions(app)
    register_blueprints(app)
    register_errorhandlers(app)
    register_shellcontext(app)
    register_commands(app)

    #minify(app=app,html=True,js=True, cssless=True,cache=True, fail_safe=True)
    return app


def register_extensions(app):
    """Register Flask extensions."""

    with app.app_context():
        bcrypt.init_app(app)
        cache.init_app(app)
        csrf_protect.init_app(app)
        db.init_app(app)
        login_manager.init_app(app)
        webpack.init_app(app)
        Analytics(app)
        Bootstrap(app)
        compress.init_app(app)
        datepicker(app)
        #run_with_ngrok(app)
    return None


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(public.views.blueprint)
    return None


def register_errorhandlers(app):
    """Register error handlers."""
    def render_error(error):
        """Render error template."""
        # If a HTTPException, pull the `code` attribute; default to 500
        error_code = getattr(error, 'code', 500)
        return render_template('{0}.html'.format(error_code)), error_code
    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)
    return None


def register_shellcontext(app):
    """Register shell context objects."""
    def shell_context():
        """Shell context objects."""
        return {
            'db': db,
            'User': user.models.User}

    app.shell_context_processor(shell_context)


def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(commands.test)
    app.cli.add_command(commands.lint)
    app.cli.add_command(commands.clean)
    app.cli.add_command(commands.urls)
